<?php
if (!empty($_GET['mid']) && isset($_GET['mid'])) {
    session_start();
    include("../../models/Customer.php");
    include("../../AuthFiles/isCustomer.php");

?>

    <!DOCTYPE html>
    <html>

    <head>
        <title>Customer | my milkman</title>
        <link rel="stylesheet" type="text/css" href="dashboard.css">
        <link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

        <script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

    </head>

    <body>

        <!-- dashboard -->
        <div class="container-fluid">
            <div class="row">
                <!-- sidebar -->
                <div class="col-md-3 text-center mainSidebar">
                    <div class="row">
                        <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%">
                            <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                            <a class="navbar-brand ml-2" href="/MDS">Dashboard</a>
                            <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </nav>
                        <div class="col-12 sidebarList ">
                            <a href="./dashboard.php">Home</a>
                        </div>
                        <div class="col-12 sidebarList ">
                            <a href="./myMilkman.php ">My Area Wise Milkman</a>
                        </div>
                        <div class="col-12 sidebarList sidebarActive">
                            <a href="./myMilkman.php">My Milkman</a>
                        </div>



                    </div>
                </div>
                <!-- sidebar end -->
                <!--  -->
                <div class="col-md-9" style="padding:0px;margin:0px">

                    <!-- Start Navigation -->
                    <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li>
                                    <?php
                                    $admin = $_SESSION['userCustomer'];
                                    echo $admin['username'];

                                    ?>
                                </li>
                            </ul>
                            <div style="float:right">
                                <button class="btn btn-primary">
                                    <a href="/MDS/models/logout.php?logout" style="color:white;">logout</a>


                                </button>
                            </div>

                        </div>
                    </nav>

                    <!-- End Navigation -->

                    <!-- Main Content -->
                    <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                        <div class="row">
                            <div class="col-12">Hi ! Welcome to <b>Milk Dairy</b></div>

                            <?php
                            $data = Customer::getMyMilkmanDetails($_GET['mid']);
                            //print_r($data);
                            ?>

                        </div>
                        <div class="row mt-4">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-8 card p-0 m-0">

                                <div class="card-header">
                                    <h3>Milkman Details</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row text-center">
                                        <div class="col-md-6 font-weight-bold">Fullname</div>
                                        <div class="col-md-6 text-secondary "><?php echo $data['fullname']; ?></div>
                                    </div>

                                    <div class="row text-center">
                                        <div class="col-md-6 font-weight-bold">Phone</div>
                                        <div class="col-md-6 text-secondary "><?php echo $data['phone']; ?></div>
                                    </div>

                                    <div class="row text-center">
                                        <div class="col-md-6 font-weight-bold">Area</div>
                                        <div class="col-md-6 text-secondary "><?php echo $data['area_code']; ?></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row mt-5 ">
                            <div class="col-md-1"></div>
                            <div class="col-md-8 p-4 bg-white">
                                <form method="POST">

                                    <div class="form-group">
                                        <label for="">Select date for cancel order</label>
                                        <input type="date" class="form-control mt-1" name="date">
                                    </div>

                                    <div class="form-group mt-4">
                                        <button class="btn btn-danger form-control" name="cancelOrder">Cancel Order</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <?php
                        $history = Customer::getMilkmanHistory($_GET['mid']);

                        ?>

                        <div class="container mt-5 mb-5">
                            <div class="row">
                                <div class="col-1"></div>
                                <div class="col text-secondary">
                                    <h3>Milk Transaction History</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-8 card p-0">
                                    <div class="card-header">
                                        <div class="row text-secondary">
                                            <div class="col"> Sr. No</div>
                                            <div class="col"> Date</div>
                                            <div class="col">Total Price</div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        $count = 1;
                                        if ($history) {
                                            foreach ($history as $dataHistoty) {
                                        ?>
                                                <div class="row bg-light p-2 m-1">
                                                    <div class="col"> <?php echo $count; ?></div>
                                                    <div class="col"> <?php echo $dataHistoty['date']; ?></div>
                                                    <div class="col"><?php echo $dataHistoty['total_price']; ?></div>
                                                </div>
                                        <?php
                                                $count++;
                                            }
                                        } else {
                                            echo '
                                                <div class="row  text-center text-secondary" >
                                                        <div class="col">
                                                        No order found !
                                                        </div>
                                                </div>
                                                ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- End Main Content -->



    </body>

    </html>

<?php
} else {
    echo '<script>
        alert("You are unauthorized");
        window.location.href="/MDS/views/customer/myMilkman.php";
        </script>';
}


if (isset($_POST['cancelOrder'])) {
    $date = $_POST['date'];
    if ($date == NULL) {
        echo '<script>
            window.alert("Please select the date");
        </script>';
    } else {
        $order = new Customer();
        $order->declineNextOrder($date, $_GET['mid']);
    }
}
?>