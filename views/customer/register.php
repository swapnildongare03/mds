<?php
include("../../models/Customer.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Customer|Registration</title>

	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

</head>
<body>
	<h3>Register here!</h3>
	<form method="POST">
		<input type="text" name="fullname" placeholder="fullname">
		<input type="text" name="username" placeholder="username">
		<input type="text" name="password" placeholder="Password">
		<input type="text" name="location" placeholder="area code">
		<input type="text" name="phone" placeholder="phone">

		<button type="submit" name="register" class="btn btn-primary">Register</button>
		
	</form>

</body>
</html>


<?php 
if(isset($_POST["register"]))
{
	$fullname = $_POST['fullname'];
	$user = $_POST['username'];
	$pass = $_POST['password'];
	$loc = $_POST['location'];
	$phone = $_POST['phone'];

    if($user == NULL || $pass == NULL || $loc == NULL || $phone == NULL || $fullname == NULL)
    {
    	echo "<script> window.alert('All feilds are required') </script>";
    }
    else
    {
    	$cust = new Customer();
    	$cust->registerCust($user,$pass,$loc,$phone,$fullname);
		//echo "USER is: $user and PASSWORD is: $pass";
    }
   
}

?>