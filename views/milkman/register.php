<?php
include("../../models/Milkman.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Milkman|Registration</title>

	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

</head>
<body>
	<h3>Register here!</h3>
	<form method="POST">
		<input type="text" name="fullname" placeholder="fullname">
		<input type="text" name="username" placeholder="username">
		<input type="password" name="password" placeholder="password">
		<input type="text" name="area_code" placeholder="area code">
		<input type="text" name="phone" placeholder="phone">

		<button type="submit" name="register" class="btn btn-primary">Register</button>
		
	</form>

</body>
</html>


<?php 
if(isset($_POST["register"]))
{
	$fullname = $_POST['fullname'];
	$user = $_POST['username'];
	$pass = $_POST['password'];
	$add = $_POST['area_code'];
	$phone = $_POST['phone'];

    if($user == NULL || $pass == NULL || $add == NULL || $phone == NULL || $fullname == NULL)
    {
    	echo "<script> window.alert('All feilds are required') </script>";
    }
    else
    {
    	$milkman = new Milkman();
    	$milkman->registerMilkman($user,$pass,$add,$phone,$fullname);
		//echo "USER is: $user and PASSWORD is: $pass";
    }
   
}

?>