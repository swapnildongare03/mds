<?php
include("DB.php");


class Customer
{
    public function registerCust($user, $pass, $loc, $phone, $fullname)
    {
        $query = DB::query("INSERT INTO `customer` (`c_id`,`fullname`, `username`, `password`, `area_code`, `phone`) VALUES (NULL,'$fullname','$user','$pass','$loc','$phone');");
        print_r($query);

        if ($query) {
            echo '<script>
	  		window.location.href = "/MDS/views/customer/login.php";
	  	 </script>';
        } else {
            echo '<script>
	  		window.alert("Please fill the data correctly");
	  	 </script>';
        }
    }

    public function getMilkmanRecords($id)
    {
        try {
            $query = DB::select("SELECT * FROM customer_milkman , milkman WHERE `c_id` = '$id'    AND `status` = 1 AND customer_milkman.mm_id=milkman.Mm_id;");
            if ($query) {
                return $query;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public static function getAllMilkRecords($id)
    {
        $query = DB::select("SELECT * FROM milk WHERE `mm_id` = $id AND availability=1 ");


        return $query;
    }




    public function isValidMilkman($mmId, $cId)
    {
        $data = DB::select("SELECT * FROM customer_milkman WHERE mm_id = $mmId AND c_id = $cId AND status = 1");
        if ($data) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public  function applyForMilkMan($mmId, $milkType, $quntity)
    {
        $date = date('Y-m-d');
        $cId = $_SESSION['userCustomer']['c_id'];
        $temp = $this->isValidMilkman($mmId, $cId);
        if ($temp) {
            if (DB::query("INSERT INTO `customer_milkman` (`id`, `c_id`, `mm_id`, `status`, `register_date`, `is_accepted`) VALUES (NULL, '$cId', '$mmId', '1', '$date', '0')")) {

                for ($i = 0; $i < count($milkType); $i++) {
                    $tempMilkType = $milkType[$i];
                    $tempQuantity = $quntity[$i];
                    DB::query("INSERT INTO `customer_milkman_milks` (`id`, `mm_id`, `c_id`, `milk_id`, `status`, `quantity`) VALUES (NULL, '$mmId', '$cId', '$tempMilkType', '0', '$tempQuantity')");
                }
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return "You have already applied for this milkman";
        }
    }


    public function isValidDateForOrderMilk($date, $mmId, $cId)
    {


        $data = DB::select("SELECT * FROM milk_order WHERE delivery_date = '$date' AND c_id = '$cId' AND mm_id = '$mmId' ");
        if ($data != NULL) {
            return FALSE;
        } else {
            return TRUE;
        }
        //return $data;
    }

    public  function declineNextOrder($date, $mmId)
    {
        $cId = $_SESSION['userCustomer']['c_id'];
        $temp = $this->isValidDateForOrderMilk($date, $mmId, $cId);
        if ($temp) {
            if (DB::query("INSERT INTO milk_order(mm_id,c_id,delivery_date,price,quantity,status) VALUES('$mmId','$cId','$date',0,0,0)")) {
                echo '<script>
                    alert("Order canceled successfully !");

                    window.location.href="/MDS/views/customer/myMilkman.php";
                 </script>';
            } else {
                echo '<script>
                    alert("Order cannot be  canceled !");

                    window.location.href="/MDS/views/customer/myMilkman.php";
                 </script>';
            }
        } else {
            echo '<script>
                    alert("Already ordered milk for this date or declined order for this date !");

                    window.location.href="/MDS/views/customer/myMilkman.php";
                 </script>';
        }
    }

    public static function getMyMilkmanDetails($mmId)
    {
        $cId = $_SESSION['userCustomer']['c_id'];
        $milkman = DB::selectOne("SELECT milkman.fullname AS fullname , milkman.area_code AS area_code , milkman.phone AS phone FROM milkman,customer_milkman cmm WHERE milkman.mm_id = cmm.mm_id AND cmm.c_id = $cId AND cmm.mm_id = $mmId");
        return $milkman;
    }

    public static function getMilkmanDetails($id)
    {
        return DB::selectOne("SELECT * FROM milkman  WHERE mm_id = $id");
    }



    public static function getMyMilkmans()
    {
        $cId = $_SESSION['userCustomer']['c_id'];
        $milkman = DB::select("SELECT milkman.mm_id AS mm_id ,milkman.fullname AS fullname , milkman.area_code AS area_code , milkman.phone AS phone FROM milkman,customer_milkman cmm WHERE milkman.mm_id = cmm.mm_id AND cmm.c_id = $cId AND cmm.status = 1 AND cmm.is_accepted = 1");
        return $milkman;
    }

    public static function getMyAreaMilkmans()
    {

        $areaCode = $_SESSION['userCustomer']['area_code'];
        $milkman = DB::select("SELECT * FROM milkman  WHERE  area_code = $areaCode AND status = 1");
        return $milkman;
    }

    public static function getMilkmanHistory($mmId)
    {
        $res = null;
        $priceCount = 0;
        $cid = $_SESSION['userCustomer']['c_id'];
        $data = DB::select("SELECT * FROM milk_order WHERE mm_id = $mmId AND c_id = $cid");
        if ($data) {
            foreach ($data as $history) {
                $res[] = [
                    'date' => $history['delivery_date'],
                    'total_price' => $history['price'] * $history['quantity'],


                ];
            }
        }
        return $res;
    }
}
