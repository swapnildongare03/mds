<?php
session_start();
include("../../models/Customer.php");
include("../../AuthFiles/isCustomer.php");

?>

<!DOCTYPE html>
<html>
<head>
	<title>Customer | my milkman</title>
	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

</head>
<body>

<!-- dashboard -->
<div class="container-fluid">
    <div class="row" >
    <!-- sidebar -->
        <div class="col-md-3 text-center mainSidebar">
            <div class="row">
                <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%"> 
                    <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                    <a class="navbar-brand ml-2" href="/MDS">Dashboard</a>
                    <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
                <div class="col-12 sidebarList ">
                	<a href="./dashboard.php">Home</a>
                </div>
                <div class="col-12 sidebarList sidebarActive">
                    <a href="./myMilkman.php ">My Area Wise Milkman</a>
                </div>
                <div class="col-12 sidebarList ">
                    <a href="./myMilkman.php">My Milkman</a>
                </div>
                
                

                
            </div>        
        </div>
    <!-- sidebar end -->
    <!--  -->
    <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">  
                    <li>
                    <?php
                    $admin = $_SESSION['userCustomer']; 
                    echo $admin['username'];
                    
                ?>
                    </li>                  
                    </ul>
                    <div style="float:right">
                     <button class="btn btn-primary">
                     	<a href="/MDS/models/logout.php?logout" style="color:white;">logout</a>

							
						</button>
                    </div>
                   
                </div>
                </nav>

                <!-- End Navigation -->

                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <div class="row" >
                        <div class="col-12">Hi ! Welcome to <b>Milk Dairy</b></div>
                        
                    </div>
                    <div class="row" style="float:right;">
                    	<!--<div>
                    	<button class="btn btn-danger" name="delMilkman">
                    		<a href="#" style="color:white;">Delete Milkman</a>
                    	</button>
                        </div>-->
                        

         
                    </div>
                    <br>
                    <div class="row">

                    <div class="card-body">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                        <th scope="col">Sr. No</th>
                                        <th scope="col">Milkman name</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Action</th>

                                        </tr>
                                    </thead>
                                        <?php

                                            $count =1;
                                            $id = $admin['c_id'];
                                            $cust = new Customer();
                                           // $row = $cust->getMilkmanRecords($id);
                                           // print_r($_SESSION['userCustomer']);
                                           $row = Customer::getMyAreaMilkmans();
                                           //print_r($row);
                                        ?>
                                    <tbody>
                                        <?php
                                            if($row)
                                            {
                                            foreach($row as $Milkman)
                                            {

                                            
                                        ?>
                                        <tr>
                                        <th scope="row"><?php echo $count;?></th>
                                        <td><?php echo $Milkman['fullname'];?></td>
                                        <td><?php echo $Milkman['area_code'];?></td>
                                        <td><?php echo $Milkman['phone'];?></td>
                                        <td><a href="/MDS/views/customer/milkmanDetails.php?mid=<?php echo $Milkman['mm_id']; ?>" class="btn btn-outline-danger">Show More</a>
                                        	</td>

                                        
                                        </tr>

                                            <?php
                                            $count++;
                                        }
                                    }
                                    else{
                                        echo "<tr> 
                                        <td> </td>
                                        <td>  </td>
                                        <td>No data found!  </td>
                                        </tr> ";

                                        print_r($_SESSION['userCustomer']);
                                    }
                                            
                                            ?>
                                        
                                    </tbody>
                                </table>

                                </div>
                            </div>
                            
                </div>
                <!-- End Main Content -->



</body>
</html>