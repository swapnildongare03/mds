<?php
if (!empty($_GET['mid']) && isset($_GET['mid'])) {
    session_start();

    include("../../models/Customer.php");
    include("../../AuthFiles/isCustomer.php");

?>

    <!DOCTYPE html>
    <html>

    <head>
        <title>Customer | my milkman</title>
        <link rel="stylesheet" type="text/css" href="dashboard.css">
        <link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

        <script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

    </head>

    <body>

        <!-- dashboard -->
        <div class="container-fluid">
            <div class="row">
                <!-- sidebar -->
                <div class="col-md-3 text-center mainSidebar">
                    <div class="row">
                        <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%">
                            <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                            <a class="navbar-brand ml-2" href="/MDS">Dashboard</a>
                            <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </nav>
                        <div class="col-12 sidebarList ">
                            <a href="./dashboard.php">Home</a>
                        </div>
                        <div class="col-12 sidebarList sidebarActive">
                            <a href="./myMilkman.php ">My Area Wise Milkman</a>
                        </div>
                        <div class="col-12 sidebarList ">
                            <a href="./myMilkman.php">My Milkman</a>
                        </div>



                    </div>
                </div>
                <!-- sidebar end -->
                <!--  -->
                <div class="col-md-9" style="padding:0px;margin:0px">

                    <!-- Start Navigation -->
                    <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li>
                                    <?php
                                    $admin = $_SESSION['userCustomer'];
                                    echo $admin['username'];

                                    ?>
                                </li>
                            </ul>
                            <div style="float:right">
                                <button class="btn btn-primary">
                                    <a href="/MDS/models/logout.php?logout" style="color:white;">logout</a>


                                </button>
                            </div>

                        </div>
                    </nav>

                    <!-- End Navigation -->

                    <!-- Main Content -->
                    <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                        <div class="row">
                            <div class="col-12">Hi ! Welcome to <b>Milk Dairy</b></div>

                            <?php
                            $data = Customer::getMilkmanDetails($_GET['mid']);
                            //print_r($data);
                            ?>

                        </div>
                        <div class="row mt-4">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-8 card p-0 m-0">

                                <div class="card-header">
                                    <h3>Milkman Details</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row text-center">
                                        <div class="col-md-6 font-weight-bold">Fullname</div>
                                        <div class="col-md-6 text-secondary "><?php echo $data['fullname']; ?></div>
                                    </div>

                                    <div class="row text-center">
                                        <div class="col-md-6 font-weight-bold">Phone</div>
                                        <div class="col-md-6 text-secondary "><?php echo $data['phone']; ?></div>
                                    </div>

                                    <div class="row text-center">
                                        <div class="col-md-6 font-weight-bold">Area</div>
                                        <div class="col-md-6 text-secondary "><?php echo $data['area_code']; ?></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="container mt-5">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-8 p-0 m-0 card">
                                    <div class="card-header">Available milks</div>
                                    <div class="card-body">
                                        <?php


                                        $data = Customer::getAllMilkRecords($_GET['mid']);

                                        // print_r($data);
                                        $count = 1;
                                        if ($data != NULL) {
                                            foreach ($data as $milk) {
                                        ?>
                                                <div class="row p-2 bg-light">
                                                    <div class="col-1"> <?php echo $count; ?></div>
                                                    <div class="col">Milk Type : <?php echo $milk['type']; ?></div>
                                                    <div class="col">Fats : <?php echo $milk['fats']; ?></div>
                                                    <div class="col">price : <span class="text-success"><?php echo $milk['price']; ?></span></div>
                                                </div>
                                        <?php
                                                $count++;
                                            }
                                        } else {
                                            echo '<p class="text-secondary text-center">No data found </p>';
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-5 ">

                            <div class="col-md-1"></div>
                            <div class="col-md-8 p-4 bg-white">

                                <p class="text-secondary">Select milks</p>
                                <?php
                                if ($data != NULL) {
                                    echo '<div class="row p-2 bg-light text-secondary">
                                    <div class="col-1">  </div>
                                    <div class="col">Milk Type </div>
                                    <div class="col">Fats </div>
                                    <div class="col">price  /ltr</span></div>
                                    <div class="col">
                                      quantity
                                    </div>
                                </div>';
                                }
                                ?>
                                <form method="POST">
                                    <?php
                                    if ($data != NULL) {
                                        foreach ($data as $milk) {
                                    ?>
                                            <div class="row p-2 bg-light">
                                                <div class="col-1"> <input type="checkbox" name="milkType[]" id="" value="<?php echo $milk['milk_id']; ?>" required> </div>
                                                <div class="col"> <?php echo $milk['type']; ?></div>
                                                <div class="col"> <?php echo $milk['fats']; ?></div>
                                                <div class="col"><span class="text-success"><?php echo $milk['price']; ?></span></div>
                                                <div class="col">
                                                    <input type="text" class="form-control" placeholder=" in ltr" name="quantity[]" required>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <button class="btn btn-success form-control" name="takeOrderButton">Take Order</button>
                                        </div>
                                    <?php } //if ends here
                                    else {
                                        echo '<p class="text-secondary text-center">Try again later </p>';
                                    }
                                    ?>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- End Main Content -->



    </body>

    </html>

<?php
} else {
    echo '<script>
        alert("You are unauthorized");
        window.location.href="/MDS/views/customer/myMilkman.php";
        </script>';
}


if (isset($_POST['takeOrderButton'])) {
    $milkType = $_POST['milkType'];
    $quantity = $_POST['quantity'];
    if ($milkType == NULL || $quantity == NULL) {
        echo '<script>
            window.alert("Please select the milk ");
        </script>';
    } else {
        $order = new Customer();
        print_r($order->applyForMilkMan($_GET['mid'], $milkType, $quantity));
    }
}
?>