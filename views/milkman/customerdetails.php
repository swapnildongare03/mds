<?php
session_start();
//print_r($_SESSION['userAdmin']);
include("../../AuthFiles/isMilkman.php");
include '../../models/Milkman.php';



?>
<!DOCTYPE html>
<html>

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

    <script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>


</head>

<body>


    <!--<button class="btn btn-primary">click me</button>
-->

    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%">
                        <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2" href="/MDS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList sidebarActive">Home</div>
                    <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/customerRequests.php">Customer Requests</a>
                    </div>
                    <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/customerList.php">My Customers</a>
                    </div>
                    <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/milkList.php">My Milk</a>
                    </div>



                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li>
                                <?php


                                $user = $_SESSION['userMilkman'];
                                echo $user['fullname'];


                                ?>
                            </li>
                        </ul>
                        <div style="float:right">
                            <button class="btn btn-primary">
                                <a href="/MDS/models/logout.php?logout" style="color:white;">logout</a>
                            </button>
                        </div>

                    </div>
                </nav>

                <!-- End Navigation -->

                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <div class="row">
                        <div class="col-12">Hi ! Welcome to <b>Milk Dairy</b></div>

                        <?php
                        $data = Milkman::getMyCustomerDetails($_GET['cid']);
                        $history = Milkman::getCustomerHistory($_GET['cid']);
                        ?>

                    </div>
                    <div class="row mt-4">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-8 card p-0 m-0">

                            <div class="card-header">
                                <h3>Customer Details</h3>
                            </div>
                            <div class="card-body">
                                <div class="row text-center">
                                    <div class="col-md-6 font-weight-bold">Fullname</div>
                                    <div class="col-md-6 text-secondary "><?php echo $data['customers']['fullname']; ?></div>
                                </div>

                                <div class="row text-center">
                                    <div class="col-md-6 font-weight-bold">Phone</div>
                                    <div class="col-md-6 text-secondary "><?php echo $data['customers']['phone']; ?></div>
                                </div>

                                <div class="row text-center">
                                    <div class="col-md-6 font-weight-bold">Area</div>
                                    <div class="col-md-6 text-secondary "><?php echo $data['customers']['location']; ?></div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row mt-5 ">
                        <div class="col-md-1"></div>
                        <div class="col-md-8 p-4 bg-white">
                            <form method="POST">

                                <div class="row">

                                    <span>Select milks to deliver and cancel</span>
                                    <div class="col-12 m-1 p-2 bg-light">
                                        <div class="form-group">
                                            <?php
                                            if (!empty($data['milks'])) {
                                                foreach ($data['milks'] as $milks) {
                                            ?>
                                                    <div class="row text-center p-2 bg-white m-1">
                                                        <div class="col">
                                                            <input class="" type="checkbox" value="<?php echo $milks['milk_id']; ?>" name="milk_id[]" id="">
                                                        </div>
                                                        <div class="col">
                                                            <span class="text-secondary"><?php echo $milks['milk_type']; ?></span>
                                                        </div>
                                                        <div class="col">
                                                            <strong>Quantity : </strong><span><?php echo $milks['milk_quntity']; ?> </span>
                                                        </div>
                                                        <div class="col">
                                                            <strong>Price : </strong> <span><?php echo $milks['milk_price']; ?> </span>
                                                        </div>
                                                    </div>
                                            <?php
                                                }
                                            } else {
                                                echo '
                                                <div class="row" text-center text-secondary >
                                                        No data found !
                                                </div>
                                                ';
                                            }
                                            ?>
                                        </div>

                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-4">
                                            <button class="btn btn-danger form-control" name="cancelOrder">Cancel Orders</button>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-5">
                                        <div class="form-group">
                                            <button class="btn btn-success form-control" style="height: 60px;" name="deliverOrder">Deliver Order for today</button>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>

                    <div class="container mt-5 mb-5">
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col text-secondary">
                                <h3>Milk Transaction History</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-8 card p-0">
                                <div class="card-header">
                                    <div class="row text-secondary">
                                        <div class="col"> Sr. No</div>
                                        <div class="col"> Date</div>
                                        <div class="col">Total Price</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <?php
                                    $count = 1;
                                    if ($history) {
                                        foreach ($history as $dataHistoty) {
                                    ?>
                                            <div class="row bg-light p-2 m-1">
                                                <div class="col"> <?php echo $count; ?></div>
                                                <div class="col"> <?php echo $dataHistoty['date']; ?></div>
                                                <div class="col"><?php echo $dataHistoty['total_price']; ?></div>
                                            </div>
                                    <?php
                                            $count++;
                                        }
                                    } else {
                                        echo '
                                                <div class="row" text-center text-secondary >
                                                        No data found !
                                                </div>
                                                ';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Main Content -->
</body>

</html>

<?php

if (isset($_POST['deliverOrder'])) {
    if (isset($_POST['milk_id'])) {
        $milkId = $_POST['milk_id'];
        $cId = $_GET['cid'];
        $order =  new Milkman;
        $order->deliverOrder($milkId, $cId);
    } else {
        echo '<script>alert("please select milk to order !");window.history.back();</script>';
    }
}
if (isset($_POST['cancelOrder'])) {

    $order = new Milkman;
    $order->declineNextOrder($_GET['cid']);
    echo '<script>alert("canceled")</script>';
}
?>