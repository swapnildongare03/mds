<?php

include("../../models/Auth.php");
?>

<!DOCTYPE html>
<html>

<head>
	<title>Admin|Login</title>

	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>
	<style>
	.form-div{
		border: 1px solid #d0d0d0;
		padding: 50px;
	}
	</style>

</head>

<body>
	<div class="container" style="justify-content: center; margin-top:150px;">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-5  form-div">

				<div class="text-center">
				<h3>Admin Login</h1>
					<form method="POST">
						<div class="form-group m-2 ">
							<input type="text" name="username" class="form-control" placeholder="username">
						</div>
						<div class="form-group m-2">
							<input type="text" name="password" class="form-control" placeholder="password">
						</div>
						<button type="submit" name="loginSubmit" class="btn btn-primary">SUBMIT</button>

					</form>
				</div>
					<div class="form-group">
						
					<a href="./register.php" class="btn btn-outline-danger ">Sign Up!</a>

					</div>

			</div>
		</div>
	</div>

</body>

</html>

<?php

if (isset($_POST["loginSubmit"])) {
	$user = $_POST['username'];
	$pass = $_POST['password'];
	if ($user == NULL || $pass == NULL) {
		echo "<script> window.alert('All feilds are required') </script>";
	} else {
		$auth = new Auth();
		$auth->AuthAdmin($user, $pass);
		echo "USER is: $user and PASSWORD is: $pass";
	}
}
?>