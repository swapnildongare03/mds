<?php
session_start();

include("../../AuthFiles/isMilkman.php");
 include("../../models/Milkman.php");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Milkmam | Add Milk</title>

	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

</head>
<body>

<!-- dashboard -->
<div class="container-fluid">
    <div class="row" >
    <!-- sidebar -->
        <div class="col-md-3 text-center mainSidebar">
            <div class="row">
                <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%"> 
                    <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                    <a class="navbar-brand ml-2" href="/MilkDelivery/views/milkman/dashboard.php">Dashboard</a>
                    <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
                <div class="col-12 sidebarList">
                        <a href="/MDS/views/milkman/dashboard.php">Home</a>
                    </div>
                <div class="col-12 sidebarList">
                    <a href="/MDS/views/milkman/customerList.php">My Customers</a>
                </div>
                <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/customerRequests.php">Customer Requests</a>
                    </div>
                <div class="col-12 sidebarList sidebarActive">
                    <a href="/MDS/views/milkman/milkList.php ">My Milk</a>
                </div>
            </div>        
        </div>
    <!-- sidebar end -->
    <!--  -->
    <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">  
                    <li>
                     <?php
                    $user = $_SESSION['userMilkman'];
                    
                        echo $user['username'];
                    
                ?>
                    </li>                  
                    </ul>
                    <div style="float:right">
                    <a href="../../models/Logout.php?logout" class="btn btn-outline-secondary">Logout</a>
                    </div>
                </div>
                </nav>

                <!-- End Navigation -->





	<h3>Add Milk here!</h3>
	<!-- Main Content -->
    <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                
	<div class="row">
	<form method="POST">
      <div class="card-body">
		<div class="form-group">
            <label >Type</label>
            <input type="text" name="type" class="form-control" id="type" >
        </div>
        <div class="form-group">
            <label >Fats</label>
            <input type="text" name="fats" class="form-control" id="fats" >
        </div><div class="form-group">
            <label >Price</label>
            <input type="text" name="price" class="form-control" id="price" >
        </div><div class="form-group">
            <label >Availability</label>
            <input type="text" name="availability" class="form-control" id="availability" >
        </div>
       </div>
      <div class="card-footer">
			<button type="submit" name="submitMilk" class="btn btn-primary">Submit</button>
      </div>
	</form>
</div>
</div>
	<!--  END Main Content -->
	
	
</body>
</html>

<?php 
if(isset($_POST["submitMilk"]))
{
	$type = $_POST['type'];
	$fats = $_POST['fats'];
	$price = $_POST['price'];
	$availability = $_POST['availability'];

    $milk = new Milkman();
    if(!empty($type) && !empty($fats) && !empty($price) && !empty($availability))
        {  
           
            $id = $user['mm_id'];
            $milk->addMilk($type,$fats,$price,$availability,$id);

        }else{
            echo "<script>window.alert('please fill all fields')</script>";
        }
    







   
  
}

?>