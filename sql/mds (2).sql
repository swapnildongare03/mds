-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 06, 2021 at 10:12 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.3.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mds`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'aarti', 'Andhale@123'),
(2, 'abc', 'abc@123'),
(3, 'xyz', 'xyz');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `c_id` int(10) NOT NULL,
  `fullname` text NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` int(10) NOT NULL,
  `area_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `fullname`, `username`, `password`, `phone`, `area_code`) VALUES
(7, '', 'Sana', 'Andhale@123', 2147483647, 0),
(8, '', 'Rohan', 'Andhale@123', 12345678, 0),
(9, '', 'suraj', 'And@123', 348570948, 0),
(10, 'xyz', 'xyz', 'xyz', 1234567, 431143),
(11, 'Customer', 'customer', 'customer', 992205032, 431143);

-- --------------------------------------------------------

--
-- Table structure for table `customer_milkman`
--

CREATE TABLE `customer_milkman` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `mm_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `register_date` date NOT NULL DEFAULT current_timestamp(),
  `is_accepted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_milkman`
--

INSERT INTO `customer_milkman` (`id`, `c_id`, `mm_id`, `status`, `register_date`, `is_accepted`) VALUES
(12, 11, 8, 1, '2021-06-01', 1),
(14, 10, 10, 1, '2021-06-03', 1),
(15, 10, 8, 1, '2021-06-05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_milkman_milks`
--

CREATE TABLE `customer_milkman_milks` (
  `id` int(10) NOT NULL,
  `c_id` int(11) NOT NULL,
  `mm_id` int(11) NOT NULL,
  `milk_id` int(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `quantity` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_milkman_milks`
--

INSERT INTO `customer_milkman_milks` (`id`, `c_id`, `mm_id`, `milk_id`, `status`, `quantity`) VALUES
(9, 11, 8, 13, 1, 12),
(10, 11, 10, 14, 0, 45),
(11, 11, 10, 15, 0, 46),
(13, 10, 10, 14, 0, 45),
(14, 10, 10, 15, 1, 46),
(16, 10, 8, 13, 0, 45);

-- --------------------------------------------------------

--
-- Table structure for table `milk`
--

CREATE TABLE `milk` (
  `milk_id` int(11) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `fats` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `mm_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `milk`
--

INSERT INTO `milk` (`milk_id`, `type`, `fats`, `price`, `availability`, `mm_id`) VALUES
(1, 'cow-1 milk', 21, 35, 0, 1),
(2, 'cow-2 milk', 23, 45, 0, 1),
(3, 'Cow milk', 20, 45, 0, 2),
(5, 'buffalo-2 milk', 30.5, 45, 0, 1),
(6, 'buffalo', 34.6, 70, 0, 2),
(7, 'goat milks', 20, 36, 1, 9),
(8, 'cow milk', 45, 60, 0, 5),
(9, 'cow-3 milk', 21, 35, 0, 1),
(10, 'cow-2 milk', 23, 45, 0, 2),
(11, 'buffelow', 2, 45, 1, 1),
(12, 'cow', 1, 12, 1, 9),
(13, 'buffelow', 2, 98, 1, 8),
(14, 'horse', 2, 22, 1, 10),
(15, 'cow', 34, 862, 1, 10),
(16, 'buffelow', 4, 45, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `milkman`
--

CREATE TABLE `milkman` (
  `mm_id` int(10) NOT NULL,
  `fullname` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` int(10) NOT NULL,
  `area_code` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `milkman`
--

INSERT INTO `milkman` (`mm_id`, `fullname`, `username`, `password`, `phone`, `area_code`, `status`) VALUES
(1, '', 'Ramlal', 'Ram@123', 987654321, 0, 1),
(2, '', 'Shyam', 'Andhale@123', 765432189, 0, 1),
(3, '', 'abc', 'Andhale@123', 76543218, 0, 1),
(5, '', 'mohan', 'And@123', 234386278, 0, 1),
(6, '', 'Sharma', 'And@123', 763985730, 0, 1),
(7, 'abc', 'abc', 'abc', 123456789, 12, 1),
(8, 'xyz', 'xyz', 'xyz', 12345789, 431143, 1),
(9, 'Swapnil Dongare', 'milkman', 'milkman', 50325, 99220, 1),
(10, 'milkman', 'milkman2', 'milkman', 222, 431143, 1);

-- --------------------------------------------------------

--
-- Table structure for table `milk_order`
--

CREATE TABLE `milk_order` (
  `id` int(11) NOT NULL,
  `mm_id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `price` float NOT NULL,
  `status` tinyint(1) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `milk_order`
--

INSERT INTO `milk_order` (`id`, `mm_id`, `c_id`, `delivery_date`, `price`, `status`, `quantity`) VALUES
(11, 2, 1, '2021-06-06', 12, 1, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `customer_milkman`
--
ALTER TABLE `customer_milkman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_milkman_milks`
--
ALTER TABLE `customer_milkman_milks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `milk`
--
ALTER TABLE `milk`
  ADD PRIMARY KEY (`milk_id`),
  ADD KEY `Mm_id` (`mm_id`);

--
-- Indexes for table `milkman`
--
ALTER TABLE `milkman`
  ADD PRIMARY KEY (`mm_id`);

--
-- Indexes for table `milk_order`
--
ALTER TABLE `milk_order`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `c_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customer_milkman`
--
ALTER TABLE `customer_milkman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `customer_milkman_milks`
--
ALTER TABLE `customer_milkman_milks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `milk`
--
ALTER TABLE `milk`
  MODIFY `milk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `milkman`
--
ALTER TABLE `milkman`
  MODIFY `mm_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `milk_order`
--
ALTER TABLE `milk_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `milk`
--
ALTER TABLE `milk`
  ADD CONSTRAINT `milk_ibfk_1` FOREIGN KEY (`mm_id`) REFERENCES `milkman` (`mm_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
