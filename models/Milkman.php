<?php
include("DB.php");
class Milkman
{
    public function registerMilkman($user, $pass, $add, $phone, $fullname)
    {
        $query = DB::query("INSERT INTO `milkman` (`mm_id`,`fullname`, `username`, `password`, `area_code`, `phone`) VALUES (NULL,'$fullname','$user','$pass','$add','$phone')");


        if ($query) {
            echo '<script>
	  		window.location.href = "/MDS/views/milkman/login.php";
	  	 </script>';
        } else {
            echo '<script>
	  		window.alert("Please fill the data correctly");
	  	 </script>';
        }
    }

    public function getAllCustomerRecords()
    {

        try {

            $query = DB::select("SELECT * FROM customer ;");
            if ($query) {
                return $query;
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    public function addMilk($type, $fats, $price, $availability, $id)
    {
        $query = DB::query("INSERT INTO `milk` (`milk_id`, `type`, `fats`, `price`, `availability`,`Mm_id`) VALUES (NULL,'$type','$fats','$price','$availability','$id');");

        if ($query) {
            echo '<script> window.alert("Milk added Successfully"); </script>';

            echo '<script>
	  		      window.location.href = "/MDS/views/milkman/milkList.php";
	  	    </script>';
        } else {
            echo '<script> window.alert("Please enter data correctly"); </script>';
        }
    }


    public function getAllMilkRecords($id)
    {
        $query = DB::select("SELECT * FROM milk WHERE `mm_id` = $id");


        return $query;
    }


    public function getUpdateMilkRecords($id)
    {
        $query = DB::select("SELECT * FROM milk WHERE `milk_id` = '$id' ;");

        if ($query) {
            return $query;
        } else {
            echo '<script> window.alert("Error"); </script>';
        }
    }

    public static function myMilkStatusManager($id)
    {
        $res = DB::selectOne("SELECT * FROM milk  WHERE milk_id = $id");
        $statusToBeChange = null;

        if ($res['availability'] == 1) {
            $statusToBeChange = 0;
        } else {
            $statusToBeChange = 1;
        }
        if (DB::query("UPDATE milk SET `availability` = '$statusToBeChange'  WHERE milk_id = $id")) {
            echo '
                <script>
                    alert("status changed");
                    window.location.href = "/MDS/views/milkman/milkList.php";
                </script>
            ';
        } else {
            echo '
                <script>
                    alert("Something went wrong");
                    window.location.href = "/MDS/views/milkman/milkList.php";
                </script>
            ';
        }
    }

    public function updateMilk($type, $fats, $price, $availability, $id)
    {
        $query = DB::query("UPDATE `milk` SET `type` = '$type',`fats` = '$fats',`price` = '$price',`availability` = '$availability' WHERE `milk_id`= '$id';");
        //print_r($query);
        if ($query) {
            echo '<script> window.alert("Milk Details Updated Successfully"); </script>';

            echo '<script>
	  		      window.location.href = "/MDS/views/milkman/milkList.php";
	  	        </script>';

            // print_r($query);

        } else {
            echo '<script> window.alert("Please enter data correctly"); </script>';
        }
    }



    public function isValidDateForOrderMilk($date, $mmId, $cId)
    {
        $data = DB::select("SELECT * FROM milk_order WHERE delivery_date = '$date' AND mm_id = $mmId AND c_id = $cId");
        if ($data != NULL) {
            return FALSE;
        } else {
            return TRUE;
        }
    }


    public  function dailyMilkOrder($cId, $price)
    {
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $delivaryDate = date('m/d/Y');
        if ($this->isValidDateForOrderMilk($delivaryDate, $mmId, $cId)) {
            if (DB::query("INSERT INTO milk_order(mm_id,c_id,delivery_date,price,status) VALUES($mmId,$cId,$delivaryDate,$price,1)")) {
                return "Odered registered succefully !";
            } else {
                return FALSE;
            }
        } else {
            return "Already ordered milk for this date or declined order for today !";
        }
    }

    public static function getMyCustomers()
    {
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $customers = DB::select("SELECT cust.c_id AS cust_id ,cust.fullname AS fullname , cust.area_code AS `location`  , cust.phone AS phone  FROM customer cust, customer_milkman cm WHERE cust.c_id = cm.c_id AND  cm.mm_id = $mmId AND status = 1 AND cm.is_accepted =1");

        return $customers;
    }

    public static function getMyCustomerDetails($id)
    {
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $customers = DB::selectOne("SELECT cust.c_id AS cust_id , cust.fullname AS fullname , cust.area_code AS `location`  , cust.phone AS phone  FROM customer cust, customer_milkman cm WHERE cust.c_id = cm.c_id AND cust.c_id = $id AND cm.mm_id = $mmId AND status = 1 AND cm.is_accepted =1");
        $milks = DB::select("SELECT cmm.id AS cmm_id , cmm.c_id AS cmm_cust_id , cmm.mm_id AS cmm_mmId, cmm.milk_id AS milk_id ,cmm.quantity AS milk_quntity , milk.type AS milk_type ,milk.price AS milk_price  FROM customer_milkman_milks cmm,milk WHERE cmm.milk_id = milk.milk_id AND cmm.mm_id = milk.mm_id AND  cmm.c_id = $id AND cmm.mm_id = $mmId AND cmm.status = 1");

        return [
            'customers' => $customers,
            'milks' => $milks

        ];
    }

    public static function getCustomerDetails($cId)
    {
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $customers = DB::selectOne("SELECT cust.fullname AS fullname , cust.area_code AS area_code , cust.phone AS phone FROM customer cust,customer_milkman cmm WHERE cust.c_id = cmm.c_id AND cmm.c_id = $cId AND cmm.mm_id = $mmId");
        return $customers;
    }

    public static function getCustomerRequests()
    {
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $data = DB::select("SELECT cust.c_id AS cust_id , cust.fullname AS cust_fullname , cust.phone AS cust_phone, cust.area_code AS cust_area, cmm.quantity AS milk_quantity,milk.type AS milk_type,cust_milk.id AS cm_id,cmm.id AS cmm_id FROM customer cust,customer_milkman_milks cmm,customer_milkman cust_milk ,milk WHERE milk.milk_id = cmm.milk_id AND cust.c_id = cust_milk.c_id AND cust_milk.c_id = cmm.c_id AND cust.c_id = cmm.c_id AND cust_milk.mm_id = cmm.mm_id AND cust_milk.is_accepted = 0 AND cust_milk.status = 1 AND cust_milk.mm_id = $mmId");

        return $data;
    }

    public static function acceptCustomerRuquest($cmm_id, $cm_id, $cust_id)
    {
        $mm_id = $_SESSION['userMilkman']['mm_id'];
        if (DB::query("UPDATE customer_milkman SET `status` = 1 ,`is_accepted` = 1 WHERE id = $cm_id AND c_id = $cust_id  AND mm_id = $mm_id")) {
            if (DB::query("UPDATE customer_milkman_milks SET `status` = 1 WHERE id = $cmm_id AND c_id = $cust_id AND mm_id = $mm_id")) {
                echo '
                <script>
                    window.alert("Reuest accepted!");
                    window.location.href = "/MDS/views/milkman/customerRequests.php";
                </script>
                ';
            } else {
                echo '
                <script>
                    window.alert("can not 2 Reuest accepted!");
                    window.location.href = "/MDS/views/milkman/customerRequests.php";
                </script>
                ';
            }
        } else {
            echo '
                <script>
                    window.alert("can not 1 Reuest accepted!");
                    window.location.href = "/MDS/views/milkman/customerRequests.php";
                </script>
                ';
        }
    }

    public static function declineCustomerRequest($cmm_id, $cm_id, $cust_id)
    {
        $mm_id = $_SESSION['userMilkman']['mm_id'];
        if (DB::query("DELETE FROM customer_milkman_milks  WHERE id = $cmm_id AND c_id = $cust_id AND mm_id = $mm_id")) {
            echo '
                <script>
                    window.alert("Order declined !");
                    window.location.href = "/MDS/views/milkman/customerRequests.php";
                </script>
                ';
        } else {
            echo '
                <script>
                    window.alert("can not 1 Order declined !");
                    window.location.href = "/MDS/views/milkman/customerRequests.php";
                </script>
                ';
        }
    }


    public  function declineNextOrder($cId)
    {
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $date = date("Y-m-d");
        $temp = $this->isValidDateForOrderMilk($date, $mmId, $cId);
       
        
        if ($temp) {
            if (DB::query("INSERT INTO milk_order(mm_id,c_id,delivery_date,price,quantity,status) VALUES('$mmId','$cId','$date',0,0,0)")) {
                echo '<script>
                    alert("Order canceled successfully !");

                    window.location.href="/MDS/views/milkman/customerList.php";
                 </script>';
            } else {
                echo '<script>
                    alert("Order cannot be  canceled !");

                    window.location.href="/MDS/views/milkman/customerList.php";
                 </script>';
            }
        } else {
            echo '<script>
                    alert("Already ordered milk for this date or declined order for this date !");

                    window.location.href="/MDS/views/milkman/customerList.php";
                 </script>';
        }
    }


    public function deliverOrder($milkId, $cId)
    {
        $milkId = $milkId[0];
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $milk = DB::selectOne("SELECT * FROM milk WHERE milk_id = $milkId");
        $milkPrice = $milk['price'];
        $custMilk = DB::selectOne("SELECT * FROM customer_milkman_milks WHERE c_id = $cId AND mm_id = $mmId AND milk_id = $milkId");
        $milkQuant = $custMilk['quantity'];
        $date = date('Y-m-d');
        $temp = $this->isValidDateForOrderMilk($date, $mmId, $cId);
        if ($temp) {
            if (DB::query("INSERT INTO milk_order(mm_id,c_id,delivery_date,price,quantity,status) VALUES('$mmId','$cId','$date','$milkPrice','$milkQuant',1)")) {
                echo '<script>
                    alert("Order Delivered successfully !");

                    window.location.href="/MDS/views/milkman/customerList.php";
                 </script>';
            } else {
                echo '<script>
                    alert("Order cannot be  delivered !");

                    window.location.href="/MDS/views/milkman/customerList.php";
                 </script>';
            }
        } else {
            echo '<script>
                    alert("Already ordered milk for this date or declined order for this date !");

                    window.location.href="/MDS/views/milkman/customerList.php";
                 </script>';
        }
    }


    public static function getCustomerHistory($cid)
    {
        $res = null;
        $priceCount = 0;
        $mmId = $_SESSION['userMilkman']['mm_id'];
        $data = DB::select("SELECT * FROM milk_order WHERE mm_id = $mmId AND c_id = $cid");
        if ($data) {
            foreach ($data as $history) {
                $res[] = [
                    'date' => $history['delivery_date'],
                    'total_price' => $history['price'] * $history['quantity'],


                ];
            }
        }
        return $res;
    }
}
