<?php
session_start();
//print_r($_SESSION['userAdmin']);
include("../../AuthFiles/isMilkman.php");
include '../../models/Milkman.php';


?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

    <script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

    <title>Customer requests</title>
</head>

<body>


    <!--<button class="btn btn-primary">click me</button>
-->

    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%">
                        <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2" href="/MDS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList ">Home</div>
                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/MDS/views/milkman/customerRequests.php">Customer Requests</a>
                    </div>
                    <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/customerList.php">My Customers</a>
                    </div>
                    <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/milkList.php">My Milk</a>
                    </div>

                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li>
                                <?php


                                $user = $_SESSION['userMilkman'];
                                echo $user['fullname'];


                                ?>
                            </li>
                        </ul>
                        <div style="float:right">
                            <button class="btn btn-primary">
                                <a href="/MDS/models/logout.php?logout" style="color:white;">logout</a>
                            </button>
                        </div>

                    </div>
                </nav>

                <!-- End Navigation -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <?php
                    $data = Milkman::getCustomerRequests();
                    $count = 1;
                    ?>
                    <div class="container p-5">
                        <div class="row text-secondary">
                            <h2>Customer Rrequests</h2>
                        </div>
                        <div class="row fw-bold text-center">
                            <div class="col-1">
                                Sr. No
                            </div>
                            <div class="col-2">
                                Customer name
                            </div>
                            <div class="col-2">
                                Contact
                            </div>
                            <div class="col-1">
                                Area
                            </div>
                            <div class="col">
                                Milk type
                            </div>
                            <div class="col-1">
                                Quantity
                            </div>
                            <div class="col">
                                Action
                            </div>
                        </div>

                        <div class="container-fluid bg-white p-1 mt-2">
                            <?php

                            if ($data != NULL) {
                                foreach ($data as $customer) {

                            ?>


                                    <div class="row text-secondary text-center mt-3">
                                        <div class="col-1">
                                            <?php echo $count; ?>
                                        </div>
                                        <div class="col-2">
                                            <?php echo $customer['cust_fullname']; ?>
                                        </div>
                                        <div class="col-2">
                                            <?php echo $customer['cust_phone']; ?>
                                        </div>
                                        <div class="col-1">
                                            <?php echo $customer['cust_area']; ?>
                                        </div>
                                        <div class="col">
                                            <?php echo $customer['milk_type']; ?>
                                        </div>
                                        <div class="col-1">
                                            <?php echo $customer['milk_quantity']; ?>
                                        </div>
                                        <div class="col">
                                            <form action="" method="post">
                                                <input type="hidden" name="cust_id" value="<?php echo $customer['cust_id']; ?>"><input type="hidden" name="cm_id" value="<?php echo $customer['cm_id'] ?>"><input type="hidden" name="cmm_id" value="<?php echo $customer['cmm_id'] ?>">
                                                <button class="btn btn-primary" name="acceptReq">Accept</button>
                                                <button class="btn btn-danger" name="declineReq">Decline</button>
                                            </form>


                                        </div>
                                    </div>



                            <?php
                                    $count++;
                                }
                            } else {
                                echo '
                                    <div class="row text-secondary text-center p-5">
                                       <div class="col-12"> NO Reuest Found !</div>
                                    </div>
                                ';
                            }


                            ?>
                        </div>
                    </div>
                </div>
</body>

</html>

<?php
$mm_id = $_SESSION['userMilkman']['mm_id'];
echo $mm_id;

if (isset($_POST['acceptReq'])) {
    $cmm_id = $_POST['cmm_id'];
    $cm_id = $_POST['cm_id'];
    $cust_id = $_POST['cust_id'];



    Milkman::acceptCustomerRuquest($cmm_id,$cm_id,$cust_id);
}

if (isset($_POST['declineReq'])) {
    $cmm_id = $_POST['cmm_id'];
    $cm_id = $_POST['cm_id'];
    $cust_id = $_POST['cust_id'];

    Milkman::declineCustomerRequest($cmm_id,$cm_id,$cust_id);

}

?>