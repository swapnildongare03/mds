<?php
session_start();
include("../../AuthFiles/isMilkman.php");
include("../../models/Milkman.php");


?>
<!DOCTYPE html>
<html>
<head>
	<title>Milkman | Update Milk</title>

	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>


</head>
<body>


<!-- dashboard -->
<div class="container-fluid">
    <div class="row" >
    <!-- sidebar -->
        <div class="col-md-3 text-center mainSidebar">
            <div class="row">
                <nav class="navbar bg-light sticky-top col-12" style="padding:10px;widht:100%"> 
                    <img src="../../images/logo.png" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                    <a class="navbar-brand ml-2" href="/MilkDelivery/views/milkman/dashboard.php">Dashboard</a>
                    <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
                <div class="col-12 sidebarList sidebarActive">
                    <a href="/MDS/views/milkman/dashboard.php">Home</a>
                </div>
                <div class="col-12 sidebarList ">
                        <a href="/MDS/views/milkman/customerRequests.php">Customer Requests</a>
                    </div>
                <div class="col-12 sidebarList">
                    <a href="/MDS/views/milkman/customerList.php">My Customers</a>
                </div>
                <div class="col-12 sidebarList sidebarActive">
                    <a href="/MDS/views/milkman/milkList.php">My Milk</a>
                </div>
            </div>        
        </div>
    <!-- sidebar end -->

    <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-light col-12" style="padding:16px;">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">  
                    <li>
                     <?php
                        $user = $_SESSION['userMilkman'];
                        echo $user['username'];
                     
                    ?>
                    
                    </li>                  
                    </ul>
                    <div style="float:right">
                    <a href="../../models/Logout.php?logout" class="btn btn-outline-secondary">Logout</a>
                    </div>
                </div>
                </nav>

                <!-- End Navigation -->


                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <div class="row" >
                       <!-- <div class="col-12">Hi ! Welcome to <b>Milk Dairy</b></div>-->
                        <div class="col-12">
                            Update your Milk details here...
                        </div>
                        
                    </div>
                    <?php
                        $count =1;
                        $milk = new Milkman();
                        $data = $milk->getUpdateMilkRecords($_GET["milk_id"]);
                        //print_r($data);
                    ?>
                    <div class="row">
                        <form  method="POST">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label >Milk Id</label>
                                        <input type="text" name="milkid" class="form-control" id="milkid" value="<?php echo $data[0]['milk_id'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label >Type</label>
                                        <input type="text" name="type" class="form-control" id="type" value="<?php echo $data[0]['type'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label >Fats</label>
                                        <input type="text" name="fats" class="form-control" id="fats"   value="<?php echo $data[0]['fats'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label >Price</label>
                                        <input type="text" name="price" class="form-control" id="price"  value="<?php echo $data[0]['price'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label >Availability</label>
                                        <input type="text" name="availability" class="form-control" id="availability" value="<?php echo $data[0]['availability'] ?>">
                                    </div>
                                   
                                <div class="card-footer">
                                    <button type="submit" name="updateMilk" class="btn btn-info">Update</button>
                                </div>
                                    
                                </form>
                   </div>
    
                  
                    </div>

        
                <!-- End Main Content -->
</body>
</html>

<?php
if(isset($_POST['updateMilk']))
{
    $milk_id = $_POST['milkid'];
	$type = $_POST['type'];
	$fats = $_POST['fats'];
	$price = $_POST['price'];
	$availability = $_POST['availability'];

	
    if($type == NULL || $fats == NULL || $price == NULL || $availability == NULL)
    {
        echo "<script> window.alert('All feilds are required'); </script>";
    }
    else
    {
        //foreach( $_SESSION['userMilkman'] as $user)
          //  {
            //    $id =  $user['Mm_id'];
            //}
        $milk = new Milkman();
        $milk->updateMilk($type,$fats,$price,$availability,$milk_id);
    }








    
}

?>