<?php
include("../../models/Admin.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Admin|Registration</title>

	<link rel="stylesheet" type="text/css" href="dashboard.css">
	<link rel="stylesheet" type="text/css" href="../../assets/bootstrap/css/bootstrap.min.css">

	<script type="text/javascript" src="../../assets/bootstrap/js/jquery.slim.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../assets/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../../assets/bootstrap/js/popper.min.js"></script>

</head>
<body>
	<h3>Register here!</h3>
	<form method="POST">
		<input type="text" name="username">
		<input type="password" name="password">
		<button type="submit" name="register" class="btn btn-primary">Register</button>
		
	</form>

</body>
</html>


<?php 
if(isset($_POST["register"]))
{
	$user = $_POST['username'];
	$pass = $_POST['password'];
    if($user == NULL || $pass == NULL)
    {
    	echo "<script> window.alert('All feilds are required') </script>";
    }
    else
    {
    	$admin = new Admin();
    	$admin->registerAdmin($user,$pass);
		//echo "USER is: $user and PASSWORD is: $pass";
    }
   
}

?>